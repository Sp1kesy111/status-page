include:
  - template: Code-Quality.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - project: gitlab-org/frontend/untamper-my-lockfile
    file: 'templates/merge_request_pipelines.yml'

workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_MERGE_REQUEST_IID
      when: always

# Ensure that the scanning is only executed on MRs and the default branch
# This potentially can be made obsolete once:
# https://gitlab.com/gitlab-org/gitlab/-/issues/217668 lands
.secure-jobs-config: &secure-jobs-config
  needs: []
  rules:
    - when: always

gemnasium-dependency_scanning:
  <<: *secure-jobs-config

code_quality:
  <<: *secure-jobs-config
  tags:
    - gitlab-org-docker

license_scanning:
  <<: *secure-jobs-config

semgrep-sast:
  <<: *secure-jobs-config

nodejs-scan-sast:
  <<: *secure-jobs-config

default:
  image: 'node:lts-alpine'
  tags:
    - gitlab-org

stages:
  - install
  - test
  - build
  - sync
  - destroy

install:yarn:
  stage: install
  script:
    - yarn install --frozen-lockfile
  cache:
    paths:
      - node_modules/
  artifacts:
    paths:
      - node_modules/
    expire_in: 1d

.aws-image:
  image:
    name: registry.gitlab.com/gitlab-org/cloud-deploy/master:eb51eaf5dc5de2bd7ae167f13c190245f2722eb5
    entrypoint: ['']

eslint:
  needs:
    - install:yarn
  stage: test
  variables:
    NODE_ENV: 'CI'
  script: yarn eslint

stylelint:
  needs: 
    - install:yarn
  stage: test
  script: yarn stylelint

test:unit:
  needs:
    - install:yarn
  stage: test
  script: yarn test:unit

build:yarn:
  needs:
    - install:yarn
  stage: build
  script: yarn build
  artifacts:
    paths:
      - dist
    expire_in: 1d

lighthouse:
  image: cypress/browsers:node14.15.0-chrome86-ff82
  needs:
    - install:yarn
  stage: test
  script:
    - npm i -g @lhci/cli
    - lhci autorun

sync:s3:
  extends: .aws-image
  stage: sync
  needs:
    - build:yarn
  script:
    - deploy/s3_setup
    - deploy/s3_sync
  only:
    variables:
      - $S3_BUCKET_NAME && $AWS_ACCESS_KEY_ID && $AWS_ACCESS_KEY_ID

destroy:s3:
  extends: .aws-image
  stage: destroy
  when: manual
  script:
    - yes | deploy/s3_teardown || true
  only:
    variables:
      - $S3_BUCKET_NAME && $AWS_ACCESS_KEY_ID && $AWS_ACCESS_KEY_ID
