import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import Logo from '~/components/logo.vue';

describe('Logo.vue', () => {
  let wrapper;

  function mountComponent() {
    const $route = {
      path: '/',
    };

    wrapper = shallowMount(Logo, {
      stubs: { RouterLink: RouterLinkStub },
      mocks: { $route },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findHomeLink = () => wrapper.findComponent(RouterLinkStub);

  it('renders the components', () => {
    expect(wrapper.element).toMatchSnapshot();
  });

  it('renders main components with link', () => {
    expect(findHomeLink().exists()).toBe(true);
  });
});
